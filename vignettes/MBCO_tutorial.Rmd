---
title: "MBCO Test Tutorial"
author: "Davood Tofighi"
output: rmarkdown::html_vignette
date: "`r Sys.Date()`"
vignette: >
  %\VignetteIndexEntry{MBCO Test Tutorial}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}{inputenc}
---

```{r, echo = FALSE, message = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
   message = FALSE,
  strip.white = TRUE
)
```

```{r setup}
  library(RMediation)
  library(OpenMx)
  library(tidyverse)
  data("memory_exp")
  mxOption(NULL, "Default optimizer", "SLSQP")
```

In this section we discuss in detail the steps required to analyze our empirical example and re-produce the results. You can run the code below to reproduce the results discussed the empirical example section. The R Markdown file that contains all the code as well as the data set for the empirical example is available from the following link ( <http://gofile.me/3JPpt/qemrtp7Sp> ). When using the data, please cite the relevant study by MacKinnon et al. (2018).

## Research Question 1

For this research question, we were interested in testing whether the indirect effect through Imagery is significant. We use the single-mediator  model in Figure 1 to answer this question. We follow the three steps outlined in the manuscript to compute the MBCO test and fit indices.

### Step 1 
We first specify the alternative model, which is the single mediator model, in OpenMx. The following R script specifies the alternative model.

```{r single_med_alt}
endVar <- c('imagery', 'recall') # name the endogenous variables
maniVar <-
  c('x', 'imagery', 'recall') # name the osbserved variables

single_med <- mxModel(
  "Model 1",
  type = "RAM",
  manifestVars = maniVar,
  #specify the manifest (oberved) variables
  mxPath(
    from = "x",
    to = endVar,
    arrows = 1,
    free = TRUE,
    values = .2,
    labels = c("b1", "b3") # specify the path from X to the M (Imagery) and Y (recall)
  ),
  mxPath(
    from = 'imagery',
    to = 'recall',
    arrows = 1,
    free = TRUE,
    values = .2,
    labels = "b2" # Specify the path from M to Y
  ),
  mxPath(
    from = maniVar,
    arrows = 2,
    free = TRUE,
    values = .8,
    labels = c("s2x", "s2em", "s2ey") # Specify (residial) variances for the observed variables
  ),
  mxPath(
    from = "one",
    to = endVar,
    arrows = 1,
    free = TRUE,
    values = .1, 
    labels = c("int1","int2") # Specify the intercepts for the endogenous variables
  ),
  mxAlgebra(b1 * b2, name = "ind"),
  # define the indirect effect and call it ind
  mxData(observed = memory_exp, type = "raw") # specify the data set for analysis
)
single_med <- mxRun(single_med) # run the single mediator null model
stat_Model1 <- summary(single_med) # saving the results
stat_Model1 # rinting resuslts of the full model
```

### Step 2
We then specify the null model, which is the single mediator model with the constraint $\beta_1 \beta_2 =0$. It should be noted that in OpenMx, we do not need specify the null model from scratch. We create the null model from the alternative model by adding *update* $\beta_1 \beta_2 =0$ as a non-linear constraint as follows:

```{r single_med_null}
## The  Model is obtained by contraining indirect effect: b1*b2 to zero
single_med_null <- mxModel(model = single_med,
                           name = "Model 2",
                           mxConstraint(ind == 0, name = "b1b2_equals_0")) # non-linear constraint
single_med_null <- mxRun(single_med_null)  # Run the model
stat_1M_null <-
  summary(single_med_null) # saving summary statatistics
stat_1M_null 
```

###Step 3
The third is to compute MBCO test likelihood ratio test by comparing the likelihood between the two models, or more precisely, by computing the difference in twice log-likelihood between the two models. We can use the function `code anova` to compute the likelihood ratio test as follows:

```{r mbco_1M}
anova(single_med_null, single_med) 
```

The first row of the above table show the results for the single mediator (alternative) model. As can be seen, $-2LL$ = `r -2*round(logLik(single_med),3)`, $df=1099$, AIC = `r round(AIC(single_med),3)`. We can also use the following code to compute $LL$, AIC, and BIC:

```{r stats_1M}
logLik(single_med)  # computes LL for the alternative model
AIC(single_med) # computes AIC
BIC(single_med) # computes BIC
```
The second of the of the above table shows the comparison between the two models, including the MBCO chi-squared test in the column "diffLL"= $-2\widehat{LL}_{null} + 2 \widehat{LL}_{alternative}= 4378.270 - 4305.727 = 72.543$, and the $df$ for the chi-squared test is in the column "diffdf"=1, and the $p$-value = 1.64 E-17.

## Monte Carlo CI

Below we used the RMediation package to compute the 95\% Monte Carlo CI for the indirect effect estimate for single-mediator model:

```{r MC_ci_1M}
Mu <- coef(single_med) # path coefficient estimates
Sigma <- vcov(single_med) #covariance matrix of the parameter estimates
## MC CI for indirect effect a1*b1
mc_ci1 <- ci(mu = Mu,
Sigma = Sigma,
quant = ~ b1 * b2)
mc_ci <- unlist(mc_ci1)
cat(" Monte Carlo CI for b1*b2:\n")
knitr::kable(mc_ci)
```

## Research Question 2

The second question was, does the experiment increase use of repetition to improve memory over and above the indirect effect through imagery?  

### Step 1
We first estimate the two-mediator model in Figure 2. In this model, the two indirect effects associated with Imagery and Repetition are freely estimated. 

```{r two_med_alt}
endVar <- c('imagery', 'repetition', 'recall')
maniVar <- c('x', 'imagery', 'repetition', 'recall')
two_med <- mxModel(
  "Model 3",
  type = "RAM",
  manifestVars = maniVar,
  mxPath(
    from = "x",
    to = endVar,
    arrows = 1,
    free = TRUE,
    values = .2,
    labels = c("b1", "b3", "b5")
  ),
  mxPath(
    from = 'repetition',
    to = 'recall',
    arrows = 1,
    free = TRUE,
    values = .2,
    labels = 'b4'
  ),
  mxPath(
    from = 'imagery',
    to = 'recall',
    arrows = 1,
    free = TRUE,
    values = .2,
    labels = "b2"
  ),
  mxPath(
    from = maniVar,
    arrows = 2,
    free = TRUE,
    values = .8,
    labels = c("s2x", "s2em1", "s2em2", "s2ey")
  ),
  mxPath(
    from = 'imagery',
    to = 'repetition',
    arrows = 2,
    free = TRUE,
    values = .2,
    labels = "cov_m1m2"
  ),
  mxPath(
    from = "one",
    to = endVar,
    arrows = 1,
    free = TRUE,
    values = .1,
    labels = c("int1", "int2", "int3")
  ),
  mxAlgebra(b1 * b2, name = "ind1"),
  mxAlgebra(b3 * b4, name = "ind2"),
  mxData(observed = memory_exp, type = "raw")
)

two_med <- mxTryHard(two_med) # run the single mediator null model
stat_Model3 <- summary(two_med) # saving the results
stat_Model3 # rinting resuslts of the full model

```
